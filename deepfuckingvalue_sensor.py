import datetime
import logging
import json
import multiprocessing
import os
import time

import multiprocessing_logging

import requests

from dotenv import load_dotenv

from twilio.rest import Client, TwilioException


class DeepfuckingvalueSensor:
    def __init__(self):
        self.api_base_permalink = "https://oauth.reddit.com/"
        self.targeted_user = "deepfuckingvalue"
        self.cycle_time = 5
        self.last_deepfuckingvalue_post_creation_date = None
        self.last_deepfuckingvalue_post_permalink = None
        self.api_access_token = None
        self.api_user_agent = "deepfuckingvalue-sensor by HaveYouSeenRaph"

    def get_reddit_api_token(self):
        base_permalink = "https://www.reddit.com/"

        data = {
            "grant_type": "password",
            "username": os.getenv("DFV_SENSOR_USERNAME"),
            "password": os.getenv("DFV_SENSOR_PASSWORD"),
        }

        auth = requests.auth.HTTPBasicAuth(
            os.getenv("DFV_SENSOR_APP_ID"), os.getenv("DFV_SENSOR_APP_SECRET")
        )

        try:
            response = requests.post(
                base_permalink + "api/v1/access_token",
                data=data,
                headers={"user-agent": self.api_user_agent},
                auth=auth,
            )
        except requests.exceptions.RequestException as e:
            logging.error("Can not get reddit token. Error: %s", e)
            return -1

        if response.status_code == 200 and "access_token" in response.json():
            self.api_access_token = response.json()["access_token"]
            logging.info("API token recovered: %s", self.api_access_token)
        else:
            self.raise_error(
                f"Can't get API token. HTTP response status: {response.status_code}"
            )

        return response.status_code

    def request_last_post(self):
        token = "bearer " + self.api_access_token
        headers = {"Authorization": token, "User-Agent": self.api_user_agent}

        try:
            response = requests.get(
                self.api_base_permalink
                + f"user/{self.targeted_user}/overview/?limit=1",
                headers=headers,
            )
            return response

        except requests.exceptions.RequestException as e:
            logging.error("Can not get reddit last post. Error: %s", e)
            return None

    def process_response_json(self, response_json):
        if "data" in response_json and response_json["data"]:
            last_post = response_json["data"]["children"][0]
            if "data" in last_post:
                if not self.last_deepfuckingvalue_post_creation_date:
                    self.last_deepfuckingvalue_post_creation_date = last_post["data"][
                        "created_utc"
                    ]
                    self.last_deepfuckingvalue_post_permalink = last_post["data"][
                        "permalink"
                    ]

                    logging.info(
                        "Initialised! Date: %s permalink: %s",
                        self.last_deepfuckingvalue_post_creation_date,
                        self.last_deepfuckingvalue_post_permalink,
                    )
                elif (
                    self.last_deepfuckingvalue_post_creation_date
                    and self.last_deepfuckingvalue_post_creation_date
                    != last_post["data"]["created_utc"]
                ):
                    self.last_deepfuckingvalue_post_creation_date = last_post["data"][
                        "created_utc"
                    ]
                    self.last_deepfuckingvalue_post_permalink = last_post["data"][
                        "permalink"
                    ]
                    self.raise_update(last_post["kind"])
                else:
                    logging.debug("No new post")

            else:
                self.raise_error("Error response malformed")
        else:
            self.raise_error("Error response empty")

    def last_post_detection_loop(self):
        while 1:
            if self.api_access_token:
                response = self.request_last_post()

                if response:
                    try:
                        self.process_response_json(response.json())
                    except json.decoder.JSONDecodeError as e:
                        logging.error("Error when parsing response in JSON: %s", e.msg)
                elif response is not None:
                    self.api_access_token = None
                    logging.warn(
                        "Error when reaching reddit. HTTP response status: %s",
                        response.status_code,
                    )
                else:
                    logging.critical(
                        "Not a conventional error when reaching Reddit",
                    )

                time.sleep(self.cycle_time)
            else:
                status_code = self.get_reddit_api_token()
                if status_code != 200 or not self.api_access_token:
                    logging.info("Retrying in 60 seconds")
                    time.sleep(60)

    def raise_error(self, error_message: str):
        logging.error(error_message)

    def raise_update(self, kind: str):
        if kind == "t3":
            logging.info(
                "New post! Date: %s permalink: %s",
                self.last_deepfuckingvalue_post_creation_date,
                self.last_deepfuckingvalue_post_permalink,
            )

            sms_only_list = []
            sms_and_call_list = []

            try:
                with open("./subscribers_lists.json") as subscribers_file:
                    subscribers_json = json.load(subscribers_file)
                    sms_only_list = subscribers_json["sms_only_list"]
                    sms_and_call_list = subscribers_json["sms_and_call_list"]
            except:
                logging.error("Error when reading the subscribers file")

            messages_process = multiprocessing.Process(
                target=self.send_messages, args=(sms_only_list + sms_and_call_list,)
            )
            calls_process = multiprocessing.Process(
                target=self.make_calls, args=(sms_and_call_list,)
            )

            messages_process.start()
            calls_process.start()

            messages_process.join()
            calls_process.join()

        else:
            logging.info("News but not a post!")

    def send_messages(self, targets: list):
        last_post_date = datetime.datetime.fromtimestamp(
            self.last_deepfuckingvalue_post_creation_date
        )
        for target in targets:
            self.send_message(
                target,
                (
                    f"u/DeepFuckingValue post at {last_post_date}"
                    f", link: https://reddit{self.last_deepfuckingvalue_post_permalink}"
                ),
            )

    def send_message(self, target: str, body: str):
        try:
            twilio_client = Client(
                os.getenv("TWILIO_ACCOUNT_SID"), os.getenv("TWILIO_AUTH_TOKEN")
            )
            message = twilio_client.messages.create(
                body=body, from_="+15168742637", to=target
            )
            logging.info("Message sent at %s", message.date_sent)
        except TwilioException as e:
            logging.error("Can not send message. Error: %s", e.args)
        except requests.ConnectionError as e:
            logging.error("Can not contact Twilio API. Error: %s", e)

    def make_calls(self, targets: list):
        for target in targets:
            self.make_call(target)

    def make_call(self, target: str):
        try:
            twilio_client = Client(
                os.getenv("TWILIO_ACCOUNT_SID"), os.getenv("TWILIO_AUTH_TOKEN")
            )
            call = twilio_client.calls.create(
                url="https://handler.twilio.com/twiml/EH20edea62dd0c5a79ee16625e99ce4f91",
                from_="+15168742637",
                to=target,
            )
            logging.info("Call created. Status %s", call.status)
        except TwilioException as e:
            logging.error("Can not call. Error: %s", e.args)
        except requests.ConnectionError as e:
            logging.error("Can not contact Twilio API. Error: %s", e)


if __name__ == "__main__":

    logging.basicConfig(
        format=(
            "[%(asctime)-15s][%(levelname).1s] %(filename)s"
            ":%(lineno)s - %(funcName)s() - %(message)s"
        ),
        level=logging.DEBUG,
    )
    multiprocessing_logging.install_mp_handler()

    load_dotenv()

    dfv_sensor = DeepfuckingvalueSensor()
    dfv_sensor.last_post_detection_loop()
