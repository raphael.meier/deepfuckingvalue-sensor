FROM alpine:3.12

WORKDIR /deepfuckingvalue-sensor/

RUN apk add python3 py3-pip

COPY ./requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY ./deepfuckingvalue_sensor.py ./deepfuckingvalue_sensor.py

CMD [ "python3", "deepfuckingvalue_sensor.py" ]