# Deepfuckingvalue Sensor

This is a sensor that detects when DFV post a new post.

## Requirements

You must create an account and register an app for reddit API and fill the .env file.
You must also create a Twillio account in order to make phone calls and send SMS.

## Tips

Do not track change on `subscribers_lists.json`, you don't want your customers to be in clear in this repo.

```
git update-index --assume-unchanged subscribers_lists.json
```